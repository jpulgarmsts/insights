# Insights Python Script

Obtain CloudWatch Logs Insights information through command line

## What does this solve?
Often when debugging using CloudWatch Logs Insights, you need to do two separate queries.  The first query gets the error message which usually contains a GUID.  The second query uses this GUID to get a full picture of the error.

So for example, you could do a query that returns all the 400 errors.  Then the second query would return more information for all of those instances.

## Install Python 3 Dependencies
```
pip3 install pytz colorama
```

## Usage
```
> aws-mfa
> python3 insights.py project/environment/config.yml
> python3 insights.py unipay/prod/api-400.yml
```

![alt text](img/terminal.png "Terminal Output")

## YML Config Files
Here's a sample config file:
![alt text](img/sample_config.png "Sample YML Config")

### Variables

* **log_group_names:** The list of log groups to be queried. You can include up to 20 log groups.
* **incidents_query:** The CloudWatch Log Insights query.  The only requirement is that the query return a meaningful GUID from the logs which is tied to a particular process or job id that you want to get more information about.
* **incidents_limit:** total # of results you want returned for this first query
* **details_query:** The CloudWatch Log Insights query.  The second query where you get "the full picture" from the GUIDs returned on the first query.  It's important to note the $1.  This will get replaced with a string array of all the GUIDs returned on the first query.
* **guid_field:** the value we are using for our GUID in the query
* **time_field:** the time value we are using in the query
* **time_format:** the Python format of the time (so we can transform to CST)
* **message_field:** the field containing the log message
* **hours:** the number of hours into the past we want to query