import argparse, boto3, json, textwrap, time, sys, yaml
from datetime import datetime, timedelta
from pytz import timezone
from colorama import Fore, Back, Style

client = boto3.client('logs')

my_parser = argparse.ArgumentParser(description='Show Cloudwatch Insights Logs')
my_parser.add_argument('Config', metavar='config', type=str, help='YAML configuration file')
args = my_parser.parse_args()
with open(args.Config, 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

incidents_query = cfg['incidents_query']
details_query = cfg['details_query']
log_group_names = cfg['log_group_names']
hours_from_now = cfg['hours']
guid_field = cfg['guid_field']
time_field = cfg['time_field']
time_format = cfg['time_format']
message_field = cfg['message_field']

print(Fore.GREEN + "Log names:\n" + Style.RESET_ALL + str(log_group_names) + "\n")

incidents_query = incidents_query.replace("$1", str(cfg['incidents_limit']))
print(Fore.GREEN + "Running query:\n" + Style.RESET_ALL + incidents_query + "\n")

start_time = time.time()
start_query_response = client.start_query(
    logGroupNames=log_group_names,
    startTime=int((datetime.today() - timedelta(hours=hours_from_now)).timestamp()),
    endTime=int(datetime.now().timestamp()),
    queryString=incidents_query,
)

query_id = start_query_response['queryId']
response = None

while response == None or response['status'] == 'Running':
    time.sleep(0.5)
    response = client.get_query_results(
        queryId=query_id
    )
print("--- Query took %s seconds ---" % (time.time() - start_time))

# print(json.dumps(response, indent=2)) # for debugging
REQUEST_GUIDS = set()
for result in response['results']:
    for singleresult in result:
        if (singleresult['field'] == guid_field):
            REQUEST_GUIDS.add(singleresult['value'])

details_query = details_query.replace("$1", str(list(REQUEST_GUIDS)))
print(Fore.GREEN + "\nRunning query:\n" + Style.RESET_ALL + details_query + "\n")

start_time = time.time()
start_query_response = client.start_query(
    logGroupNames=log_group_names,
    startTime=int((datetime.today() - timedelta(hours=hours_from_now)).timestamp()),
    endTime=int(datetime.now().timestamp()),
    queryString=details_query,
)

query_id = start_query_response['queryId']
response = None

while response == None or response['status'] == 'Running':
    time.sleep(0.5)
    response = client.get_query_results(
        queryId=query_id
    )
print("--- Query took %s seconds ---" % (time.time() - start_time))

print(Fore.GREEN + "\nResults:" + Style.RESET_ALL, end="")
# print(json.dumps(response, indent=2))  # for debugging
last_guid = ''
show_date = True
for result in response['results']:
    for singleresult in result:
        if (singleresult['field'] == guid_field):
            if (last_guid != singleresult['value']):
                # print(Fore.GREEN + "\nRequest GUID: " + Style.RESET_ALL + singleresult['value'], end = " ")
                last_guid = singleresult['value']
                show_date = True
        if ((singleresult['field'] == time_field) and (show_date == True)):
            datetime_object = datetime.strptime(singleresult['value'], time_format)
            datetime_obj_utc = datetime_object.replace(tzinfo=timezone('UTC'))
            datetime_obj_cst = datetime_obj_utc.astimezone(timezone('US/Central'))
            print(Style.BRIGHT + '\n' + datetime_obj_cst.strftime("%Y-%m-%d %I:%M:%S%p") + " (CST)" + Style.RESET_ALL)
            show_date = False
        if (singleresult['field'] == message_field):
            print(textwrap.fill('- ' + singleresult['value'].strip(),
                    initial_indent='',
                    subsequent_indent=' ' * 4,
                    width=100,
                    ))




# Partner Integrations
# /aws/lambda/presto-receive-o-prod*
# fields @timestamp, @message
# | filter @message like /Error/
# | sort @timestamp desc
# | limit 200

# print(', '.join(REQUEST_GUIDS))
# fields @timestamp, @message | sort @timestamp desc | limit 25

# FROM ME
# parse log ', [*] * -- : [*] [*] *' as date, type, server, uniqueid, servermessage
# | filter server like /api.prod.unipay.msts.com/ and servermessage like /Completed 5/
# | sort @timestamp desc

# then you can get more info by:
# parse log '[0d24912f46961ac9acc8104cc06f486a] *' as servermessage
# | filter log like /0d24912f46961ac9acc8104cc06f486a/
# | sort @timestamp asc

# FROM JAMES
# parse message '*, [* #*] * -- : [*] [*] *' as level_short, log_time, pid, level, hostname, request_guid, log_message
# | filter log_message like /(Completed 4\d\d )/
# | sort @timestamp desc
# | limit 20
# | display request_guid, log_message

# parse message '*, [* #*] * -- : [*] [*] *' as level_short, log_time, pid, level, hostname, request_guid, log_message
# | filter request_guid in ['55c2af76cbc8016630172828c3f4fd40','590c4ff572b8d391f3e83cc7968ca2e2']
# | sort request_guid, @timestamp asc
# | limit 20
# | display request_guid, log_message
# The command line uses the same syntax as the non-insights search where the syntax is much more restricted, to search for 400 errors for example you would use { $.message = "*Completed 4*" }
# parse message '*, [* #*] * -- : [*] [*] *' as level_short, log_time, pid, level, hostname, request_guid, log_message
# | filter request_guid in ['9ca9ab8899191d19b3ee6a10be279cfb']
# | sort request_guid, @timestamp asc
# | limit 20
# | display request_guid, log_message